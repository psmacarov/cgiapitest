from django.shortcuts import render
from django.views.generic import TemplateView
from .forms import CmdPostForm
import requests

class HomePageView(TemplateView):
    template_name = 'home.html'


def get_cmd(request):
    sent = 'N/A'
    if request.method == 'POST':
        form = CmdPostForm(request.POST)
        if form.is_valid():
           cd = form.cleaned_data
           res = requests.get(f"https://blooming-tor-05996.herokuapp.com/cgi-bin/hello.py?action={cd['cmd']}") 
           answer = res.json()
           if  answer['success'] == 'true':
               sent =  answer['answer'] 
    else: 
        form = CmdPostForm()

    return render(request, 'home.html', {'form': form,
                                         'sent': sent})

