from django import forms

class CmdPostForm(forms.Form):
    cmd = forms.CharField(label='Put command to CGI - file:', max_length=25)